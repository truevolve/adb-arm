FROM arm32v7/ubuntu:18.04

MAINTAINER Steyn Geldenhuys <steyn@truevolve.techonology>

RUN apt update && apt install -y android-tools* && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -m 0750 /root/.android

# Expose default ADB port
EXPOSE 5037
CMD adb -a -P 5037 fork-server server
